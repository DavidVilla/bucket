#!/usr/bin/make -f
# -*- mode:makefile -*-


install: build
	sudo pip install --upgrade dist/*.gz

uninstall:
	sudo pip uninstall -y bucket

build: clean
	python setup.py sdist

release:
	python setup.py sdist upload
	hg ci -m "new release"
	hg push

clean:
	$(RM) -r doc/_build build dist bucket.egg-info MANIFEST

vclean:
	$(RM) -r .svn debian MANIFEST
