# -*- coding: utf-8; mode: python -*-

public_repos = ['hg.public', 'git.public']
private_repos = ['hg.private', 'git.private']
all_repos = public_repos + private_repos
