# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import os
from unittest import TestCase
import StringIO

import commodity.args
from commodity.pattern import Bunch

from libbucket import cli, api
import libbucket.exceptions as exc
import utils
from utils import make_new_dir

from repos import public_repos, all_repos

USER = 'arco_test2'
PASS = utils.read_file('PASS')


def dummy_auth():
    return Bunch(account="{}:{}".format(USER, PASS))


class AuthTests(TestCase):
    def setUp(self):
        reload(commodity.args)
        cli.args = self.args = commodity.args.args
        self.sut = commodity.args.parser
        self.fd = StringIO.StringIO()

    def test_user_pass_from_commandline(self):
        cli.parse_args("-c /dev/null -a {0}:{1} info {0}/hg.private".format(
            USER, PASS), stdout=self.fd)

        self.args.func()

        self.assertIn('hg.private', self.fd.getvalue())

    def test_select_account_from_commandline_pass(self):
        config_file = '/tmp/bucket.conf'
        utils.create_dummy_config(config_file)

        cli.parse_args("-c {} -a testuser info {}/git.private".format(
            config_file, USER), stdout=self.fd)

        self.args.func()

        self.assertIn('git.private', self.fd.getvalue())

    def test_ask_password(self):
        cli.get_password = lambda x: PASS
        cli.parse_args("-c /dev/null -a {0} info {0}/hg.private".format(USER), stdout=self.fd)

        self.args.func()

        self.assertIn('hg.private', self.fd.getvalue())

    def test_ask_password_empty_password(self):
        cli.get_password = lambda x: PASS
        cli.parse_args("-c /dev/null -a {0}: info {0}/hg.private".format(USER), stdout=self.fd)

        self.args.func()

        self.assertIn('hg.private', self.fd.getvalue())

    def test_ask_password_empty_password_in_config_file(self):
        config_file = '/tmp/bucket.conf'
        account = api.Account(USER, '')
        utils.create_dummy_config(config_file, account)

        cli.get_password = lambda x: PASS
        cli.parse_args("-c {} -a testuser info {}/hg.private".format(
            config_file, USER), stdout=self.fd)

        self.args.func()

        self.assertIn('hg.private', self.fd.getvalue())


class InfoTests(TestCase):
    def setUp(self):
        self.reset_args()
        self.sut = commodity.args.parser
        self.fd = StringIO.StringIO()

    def tearDown(self):
        prj_root = os.path.dirname(os.path.dirname(__file__))
        os.chdir(prj_root)

    def reset_args(self):
        reload(commodity.args)
        cli.args = self.args = commodity.args.args

    def test_specify_owner_and_slug(self):
        cli.parse_args("-c {} info arco_test2/hg.private",
                       stdout=self.fd, ns=dummy_auth())

        self.args.func()

        self.assertIn('hg.private', self.fd.getvalue())

    def test_specify_slug__owner_is_account_user(self):
        cli.parse_args("-c {} info /hg.private",
                       stdout=self.fd, ns=dummy_auth())

        self.args.func()

        self.assertIn('hg.private', self.fd.getvalue())

    def test_missing_repo(self):
        cli.parse_args("-c /dev/null info /missing", stdout=self.fd, ns=dummy_auth())

        with self.assertRaises(exc.NoSuchRepo):
            self.args.func()

    def test_use_current_dir(self):
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        cli.parse_args('-c /dev/null clone {}/hg.public --destdir {}'.format(USER, destdir))
        self.args.func()

        self.reset_args()
        os.chdir(os.path.join(destdir, 'hg.public'))

        cli.parse_args('-c /dev/null info', stdout=self.fd)
        self.args.func()

        self.assertIn('hg.public', self.fd.getvalue())


class Base(TestCase):
    __test__ = False

    def setUp(self):
        self.reset_args()
        self.fd = StringIO.StringIO()

        self.account = utils.dummy_account()
        self.session = api.Session(account=self.account)
        self.manager = api.RepoManager(self.account)

        self.sut = commodity.args.parser

    def reset_args(self):
        reload(commodity.args)
        cli.args = self.args = commodity.args.args


class ListTests(Base):
    __test__ = True

    def assert_items(self, fd, items):
        output = fd.getvalue().strip()
        output = [x for x in output.split('\n')if not x.startswith('--')]

        print output
        self.assertEquals(len(items), len(output))

        for i in items:
            self.assertIn(USER + '/' + i, output)

    def test_public_repos_of_explicit_user(self):
        cli.parse_args("-c /dev/null ls {}".format(USER), stdout=self.fd)

        self.args.func()

        self.assert_items(self.fd, public_repos)

    def test_all_repos_of_default_account_user(self):
        cli.parse_args("-c /dev/null ls", stdout=self.fd, ns=dummy_auth())

        self.args.func()

        self.assert_items(self.fd, all_repos)

    def test_all_repos_of_default_account_user_with_config(self):
        config_file = '/tmp/bucket.conf'
        utils.create_dummy_config(config_file)

        cli.parse_args("-c {} ls".format(config_file), stdout=self.fd)

        self.args.func()

        self.assert_items(self.fd, all_repos)


class CreateTests(Base):
    __test__ = True

    def repo_exists(self, repo):
        try:
            api.Repo.from_remote(self.manager,
                               self.account.username + '/' + repo)
            return True
        except exc.NoSuchRepo:
            return False

    def repo_delete(self, repo):
        self.manager.repo_delete(self.account.username + '/' + repo)

    def test_create_repo_with_default_attributes(self):
        repo = 'hg.temp'

        if self.repo_exists(repo):
            self.repo_delete(repo)

        cli.parse_args('-c /tmp/bucket.conf create --yes ' + USER  + '/' + repo,
                       stdout=self.fd, ns=dummy_auth())

        self.args.func()

        self.assertTrue(self.repo_exists(repo))
        self.repo_delete(repo)

    def test_create_git(self):
        repo = 'git.temp'

        if self.repo_exists(repo):
            self.repo_delete(repo)

        cli.parse_args('-c /dev/null create --yes {}/{}'.format(USER, repo),
                       stdout=self.fd, ns=dummy_auth())

        self.args.func()

        info = api.Repo.from_remote(self.manager,
                                    self.account.username + '/' + repo)

        self.assertEquals('git', info.scm)

        self.repo_delete(repo)


# FIXME
# class DeleteTests()

class CloneTests(Base):
    __test__ = True

    def test_hg_public_repo(self):
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        cli.parse_args('-c /dev/null clone {}/hg.public --destdir {}'.format(USER, destdir))

        self.args.func()

        self.assertIn(USER + '/hg.public',
                      file(os.path.join(destdir, 'hg.public/.hg/hgrc')).read())

    def test_hg_public_repo_ssh(self):
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        cli.parse_args('-c /dev/null clone {}/hg.public --destdir {} --ssh'.format(USER, destdir))

        self.args.func()

        self.assertIn(USER + '/hg.public',
                      file(os.path.join(destdir, 'hg.public/.hg/hgrc')).read())

    def test_hg_public_repo_destdir_from_config(self):
        destdir = '/tmp/repos'
        make_new_dir(destdir)

        config_file = '/tmp/bb.conf'
        utils.create_dummy_config(config_file)

        cli.parse_args('-c {} clone {}/hg.public'.format(config_file, USER))

        self.args.func()

        self.assertIn(USER + '/hg.public',
                      file(os.path.join(destdir, 'hg.public/.hg/hgrc')).read())

    def test_hg_public_repo_proto_from_config(self):
        destdir = '/tmp/bar-repos'
        make_new_dir(destdir)

        config_file = '/tmp/bb.conf'
        utils.create_dummy_config(config_file)

        with file(config_file, 'a') as fd:
            fd.write("  proto = ssh")

        cli.parse_args('-c {} clone {}/hg.public --destdir {}'.format(config_file, USER, destdir),
                       stdout=self.fd)
        self.args.func()

        self.assertIn('(hg/ssh)', self.fd.getvalue())

    def test_hg_private_repo_https(self):
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        cli.parse_args('-c /dev/null clone {}/hg.private --destdir {}'.format(USER, destdir),
                       ns = dummy_auth())

        self.args.func()

        self.assertIn(USER + '/hg.private',
                      file(os.path.join(destdir, 'hg.private/.hg/hgrc')).read())

    # def test_private_repo_ssh(self):
    #     destdir = '/tmp/foo-repos'
    #     make_new_dir(destdir)

    #     cli.parse_args('-c /dev/null clone arco_test/hg.private --destdir {} --ssh'.format(destdir),
    #                    ns = dummy_auth())

    #     self.args.func()

    #     self.assertIn('arco_test/hg.private',
    #                   file(os.path.join(destdir, 'hg.private/.hg/hgrc')).read())

    def test_clone_with_existing__same_repo(self):
        # FIXME
        pass

    def test_clone_with_existing__not_a_repo(self):
        # FIXME
        pass

    def test_clone_with_existing_other_origin(self):
        # FIXME
        pass

    def test_clone_with_existing_other_scm(self):
        # FIXME
        pass


class SyncTests(Base):
    __test__ = True

    def test_sync_all_empty_destdir(self):
        destdir = '/tmp/repos'
        make_new_dir(destdir)

        cli.parse_args('-c /dev/null sync --owner {} --destdir {}'.format(USER, destdir),
                       ns=dummy_auth())

        self.args.func()

        self.assertListEqual(sorted(os.listdir(destdir)), sorted(all_repos))
        self.assertIn('default = https://arco_test2@bitbucket.org/arco_test2/hg.private',
                      file(os.path.join(destdir, 'hg.private/.hg/hgrc')).read())

#     def test_sync_all_empty_destdir_ssh(self):
#         destdir = '/tmp/repos'
#         make_new_dir(destdir)

#         cli.parse_args('-c /dev/null sync --owner arco_test --destdir {} --ssh'.format(destdir),
#                        ns=dummy_auth())

#         self.args.func()

#         self.assertListEqual(sorted(os.listdir(destdir)), sorted(all_repos))

    def test_sync_all_already(self):
        destdir = '/tmp/repos'
        make_new_dir(destdir)

        cli.parse_args('-c /dev/null -v sync --owner {} --destdir {}'.format(USER, destdir), ns=dummy_auth())

        self.args.func()
        self.args.func()

        self.assertListEqual(sorted(os.listdir(destdir)), sorted(all_repos))

        self.assertIn('default = https://arco_test2@bitbucket.org/arco_test2/hg.private',
                      file(os.path.join(destdir, 'hg.private/.hg/hgrc')).read())

#     def test_sync_all_already_ssh(self):
#         destdir = '/tmp/repos'
#         make_new_dir(destdir)

#         cli.parse_args('-c /dev/null -v sync --owner arco_test --destdir {} --ssh'.format(destdir), ns=dummy_auth())

#         self.args.func()
#         self.args.func()

#         self.assertIn('default = ssh://hg@bitbucket.org/arco_test/hg.private',
#                       file(os.path.join(destdir, 'hg.private/.hg/hgrc')).read())
