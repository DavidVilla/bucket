#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

import os

import libbucket.api as api

USER = 'arco_test2'


def read_file(filename):
    return file(filename).read().strip()


def dummy_account():
    return api.Account(USER, password=read_file('PASS'))


def make_new_dir(dirname):
    if os.path.exists(dirname):
        os.system('rm -rf {}'.format(dirname))

    os.makedirs(dirname)


def create_dummy_config(filename, account=None):
    account = account or dummy_account()

    with file(filename, 'w') as fd:
        fd.write('''\
[ui]
  [[account]]
  default = {0}:{1}
  testuser = {0}:{1}

  [[cmd:clone]]
  destdir = /tmp/repos
'''.format(account.username, account.password))
