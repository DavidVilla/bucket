# -*- mode:python; coding:utf-8; tab-width:4 -*-

import os
from functools import partial

from unittest import TestCase
from libbucket import api

import utils
from utils import make_new_dir

from repos import public_repos, all_repos

import libbucket.exceptions as exc

USER = 'arco_test2'


class RepoTest(TestCase):
    def test_same_repo(self):
        manager = api.RepoManager()
        repo1 = api.Repo.from_remote(manager, USER + '/hg.public')
        repo2 = api.Repo.from_remote(manager, USER + '/hg.public')
        assert repo1 == repo2

    def test_distint_repos(self):
        manager = api.RepoManager()
        repo1 = api.Repo.from_remote(manager, USER + '/hg.public')
        repo2 = api.Repo.from_remote(manager, USER + '/git.public')
        assert repo1 != repo2


class ListTests(TestCase):
    def test_public_repos(self):
        repo_names = [r.slug for r in api.RepoManager().repo_list(USER)]
        self.assertItemsEqual(public_repos, repo_names)

    def test_private_repos(self):
        manager = api.RepoManager(utils.dummy_account())
        repo_names = [r.slug for r in manager.repo_list(USER)]
        self.assertItemsEqual(all_repos, repo_names)


class InfoTest(TestCase):
    def setUp(self):
        self.manager = api.RepoManager()

    def test_public_hg_info(self):
        repo = api.Repo.from_remote(self.manager, USER + '/hg.public')
        self.assertEquals('hg.public', repo.slug)
        self.assertEquals('hg', repo['scm'])

    def test_public_git_info(self):
        repo = api.Repo.from_remote(self.manager, USER + '/git.public')
        self.assertEquals('git.public', repo.slug)
        self.assertEquals('git', repo['scm'])

    def test_missing_repo(self):
        with self.assertRaises(exc.NoSuchRepo):
            api.Repo.from_remote(self.manager, USER + '/missing')


class Base(TestCase):
    __test__ = False

    def setUp(self):
        self.account = utils.dummy_account()
        self.session = api.Session(account=self.account)
        self.manager = api.RepoManager(self.account)
        self.keymanager = self.manager.key_manager

    def add_new_key(self, private_key):
        os.system('rm -f {0}*'.format(private_key))
        os.system('ssh-keygen -N "" -f {0} > /dev/null'.format(private_key))
        self.keymanager.add_key('key-0', file(private_key + '.pub'))


class CreateTests(Base):
    __test__ = True

    def remove_if_exists(self, repo):
        try:
            api.Repo.from_remote(self.manager, self.account.username + '/' + repo)
            self.manager.repo_delete(self.account.username + '/' + repo)
        except exc.NoSuchRepo:
            pass

    def checked_remove(self, repo):
        self.manager.repo_delete(self.account.username + '/' + repo)
        self.assertNotIn(repo, [x.slug for x in self.manager.repo_list(USER)])

    def test_create_repo_with_default_attributes(self):
        repo = 'foo'
        self.addCleanup(partial(self.checked_remove, repo))
        self.remove_if_exists(repo)

        self.manager.repo_create(self.account.username + '/' + repo)

        repo = api.Repo.from_remote(self.manager, self.account.username + '/' + repo)
        self.assertEquals('hg', repo.scm)
        self.assertTrue(repo.is_private())

    def test_create_hg_as_explicit_scm(self):
        reponame = 'bar'
        self.addCleanup(partial(self.checked_remove, reponame))
        self.remove_if_exists(reponame)

        self.manager.repo_create(self.account.username + '/' + reponame, scm='hg')

        repo = api.Repo.from_remote(self.manager, self.account.username + '/' + reponame)
        self.assertEquals('hg', repo.scm)

    def test_create_git(self):
        repo = 'buzz'
        self.addCleanup(partial(self.checked_remove, repo))
        self.remove_if_exists(repo)

        self.manager.repo_create(self.account.username + '/' + repo, scm='git')

        repo = api.Repo.from_remote(self.manager, self.account.username + '/' + repo)
        self.assertEquals('git', repo.scm)

    def test_create_public(self):
        repo = 'blah'
        self.addCleanup(partial(self.checked_remove, repo))
        self.remove_if_exists(repo)

        self.manager.repo_create(self.account.username + '/' + repo, is_private=False)

        repo = api.Repo.from_remote(self.manager, self.account.username + '/' + repo)
        self.assertEquals(False, repo.is_private())


class CloneTests(Base):
    __test__ = True

    def test_hg_public_https(self):
        reponame = 'hg.public'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo.clone('https', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    def test_hg_private_https(self):
        reponame = 'hg.private'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo.clone('https', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    def test_hg_public_ssh(self):
        reponame = 'hg.public'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo.clone('ssh', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    def test_hg_private_ssh(self):
        reponame = 'hg.private'
        keyfile = '/tmp/sshkey'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        self.add_new_key(keyfile)
        os.system('ssh-agent; ssh-add %s' % keyfile)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo._custom_args = ['-e', 'ssh -i /tmp/sshkey -l ' + USER]
        repo.clone('ssh', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    def test_git_public_https(self):
        reponame = 'git.public'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo.clone('https', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    def test_git_public_ssh(self):
        reponame = 'git.public'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo.clone('ssh', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    def test_git_private_https(self):
        reponame = 'git.private'
        destdir = '/tmp/foo-repos'
        make_new_dir(destdir)

        repo = api.Repo.from_remote(self.manager, USER + '/' + reponame)
        repo.clone('https', os.path.join(destdir, reponame))

        cloned_repo = api.Repo.from_dir(self.manager,
                                        os.path.join(destdir, reponame))
        self.assertEquals(repo.full_name, cloned_repo.full_name)

    # def test_git_private_ssh(self):
    #     reponame = 'git.private'
    #     keyfile = '/tmp/sshkey'
    #     destdir = '/tmp/foo-repos'
    #     make_new_dir(destdir)

    #     self.add_new_key(keyfile)
    #     os.system('ssh-agent; ssh-add %s' % keyfile)

    #     repo = api.Repo.from_remote(self.manager, 'arco_test/' + reponame)
    #     repo._custom_args = ['-e', 'ssh -i /tmp/sshkey']
    #     repo.clone('ssh', os.path.join(destdir, reponame))

    #     cloned_repo = api.Repo.from_dir(self.manager,
    #                                     os.path.join(destdir, reponame))
    #     self.assertEquals(repo.full_name, cloned_repo.full_name)


class RepoTests(Base):
    # FIXME: Repo.from_dir with a only local repo
    def test_(self):
        pass


class SshTests(Base):
    __test__ = True

    def remove_all_keys(self):
        for k in self.keymanager.list_key_labels():
            try:
                self.keymanager.delete_key_by_label(k)
            except exc.NoSuchKey:
                pass

    def test_list_keys(self):
        private_key = '/tmp/sshkey'

        self.remove_all_keys()
        self.addCleanup(self.remove_all_keys)

        self.add_new_key(private_key)
        self.assertEquals(['key-0'], self.keymanager.list_key_labels())

    def test_add_key(self):
        private_key = '/tmp/sshkey'
        public_key = private_key + '.pub'

        try:
            self.keymanager.delete_key_by_label('key-0')
        except exc.NoSuchKey:
            pass

        self.add_new_key(private_key)

        try:
            self.keymanager.add_key('key-0', file(public_key))
        except exc.NoValidKey:
            self.keymanager.delete_key_by_label('key-0')
