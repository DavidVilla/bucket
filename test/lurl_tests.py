# -*- coding: utf-8; mode: python -*-

# -*- mode:python; coding:utf-8; tab-width:4 -*-

from unittest import TestCase
from libbucket.lurl import parse


class URLTests(TestCase):
    def test_git_git_debian(self):
        url = parse('git://git.debian.org/users/brlink/xwit.git')
        self.assertEquals(url.scheme, 'git')
        self.assertEquals(url.netloc, 'git.debian.org')
        self.assertEquals(url.path,   '/users/brlink/xwit.git')

    def test_git_github(self):
        url = parse('git@github.com:CRySoL/web.source.git')
        print url
        self.assertEquals(url.scheme, 'git')
        self.assertEquals(url.netloc, 'git@github.com')
        self.assertEquals(url.path,   '/CRySoL/web.source.git')
