#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

import sys
import os
import logging

import gitapi

logging.basicConfig()
logger = logging.getLogger('bucket')
# logger.setLevel(logging.DEBUG)

import utils
import libbucket.api as api

if not os.path.exists('USER'):
    print "Create a bitbucket account. Write the username in a textfile USER"
    sys.exit(1)

if not os.path.exists('PASS'):
    print "Create a bitbucket account. Write the password in a textfile PASS"
    sys.exit(1)


account = utils.dummy_account()
manager = api.RepoManager(account)

print "This will delete all the '{}' repositories!!".format(account.username)
# cli.confirm_irrecoverable_operation()

for r in list(manager.repo_list(account.username)):
    print "deleting '{}'".format(r.full_name)
    manager.repo_delete(r.full_name)

repos = [
    ('/hg.public',   'hg',  False),
    ('/hg.private',  'hg',  True),
    ('/git.public',  'git', False),
    ['/git.private', 'git', True]
]

print "-- creating required fixture repos..."
for name, scm, is_private in repos:
    full_name = account.username + name
    print "creating '{}'".format(full_name)
    manager.repo_create(full_name, scm, is_private)

    rootdir = '/tmp/bucket-fixtures'

    if scm == 'git':
        if os.path.exists(rootdir):
            os.system('rm -rf ' + rootdir)

        os.makedirs(rootdir)

        destdir = rootdir + '/' + name

        bb_repo = api.Repo.from_remote(manager, full_name)
        bb_repo.clone('https', destdir)

        os.system('touch ' + os.path.join(destdir, '.placeholder'))

        repo = bb_repo.api_repo
        repo.git_add('*')
        repo.git_commit('initial commit')

        bb_repo.push('https')

print "-- ok"
